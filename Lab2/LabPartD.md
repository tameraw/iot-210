[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

Lab2 Part D - Multicast

Note: If working remotely, you will want to have a terminal on your PC, or have two
Raspberry Pi devices on your same Wi-Fi (or wired) network.

Broadcasts do not go to yourself.


# Step 1 - Setup UDP Server

Open an SSH Terminal into the RPi3. Run the UDP server.

```
$pi cd ~/Documents/Git/iot-210B-student/Lab2/src
$pi python ipv4_udp_server.py
```

# Step 2 - Find Multicast Address on your RPi3

Look for the Bcast interface. This isn't really a broadcast,
but a multicast on your subnet. (e.g. 172.22.194.255)

```
$pi ifconfig | grep inet
```

# Step 3 - Run UDP Client

**Open another SSH terminal** into your RPi3.

Modify the UDP client default address to broadcast. Or, alternately, just type it in as the 2nd
argument.

```
$pi$ python ipv4_udp_client.py Hello World 172.22.194.255
```

Try sending messages to each other unicast (e.g. to iot40a2.local) and broadcast to all in the room


[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

# Homework

The homework is to install the Texas Instruments ZigBee Software on your
Microsoft **Windows** laptop. If you run macOS or Linux, you'll need to
install a Windows virtual machine.

Many of the embedded tools run on Windows only (some only on older versions
such as Window 7 or even Windows XP). All the tools we use in class should
run on Windows 10 just fine.

# Step 1 - Hardware - CC2650 LaunchPad

You'll need at least one Texas Instruments CC2650 Launchpad. If working
remotely, get 2.

I recommend you order from DigiKey (as you can get it in a couple of days or less).

https://www.digikey.com/en/product-highlight/t/texas-instruments/launchxl-cc2650-launchpad-kit

See also: http://www.ti.com/tool/launchxl-cc2650

**Optionally** get an 802.15.4 Sniffer (to watch packets).

http://www.ti.com/tool/cc2531emk

Digi-Key has it labled wrong (RFID rather that 802.15.4), but this is a sniffer: 

https://www.digikey.com/product-detail/en/texas-instruments/CC2531EMK/296-28921-ND/2231688


# Step 2 - Download the SimpleLink App in your Phone

If you have a smart phone (Android or Apple), you can Download
the Texas Instruments `Simplelink Starter` app from Apple App
Store or Google Play.

If you have the CC2650 LaunchPad hardware, you can verify it all
works, as it has a Bluetooth application ready to go out-of-box.

1. Plug LaunchPad into power (you can use one of your USB chargers or your laptop
2. Launch the SimpleLink Starter app
3. Press (and hold!) the BTN-2 button on the LaunchPad
4. Select the LaunchPad from the menu of Bluetooth Smart Devices
5. Select Mission Control
6. Turn on/off LEDs
7. Try out buttons (it will indicate on screen the state of BTN-1 and 2


# Step 3 - Download and Install Texas Instruments Software

http://processors.wiki.ti.com/index.php/CC2650_LaunchPad_User%27s_Guide_for_ZigBee

Follow the instructions down to and including the "Software" heading,
including downloading and installing

* Z-Stack Home 1.2.2a SDK
* TI-RTOS for SimpleLink Wireless MCUs
* IAR for ARM (EWARM) Embedded WorkBench ARM (see IAR below)

We'll be going through the "Porting Z-Stack Home) and setting up
the actual LaunchPad boards with ZigBee programs in class.

# Step 4 - Download and Install IAR

IAR (https://www.iar.com) Embedded WorkBench for ARM is a C compiler
and debugging environment. ARM processors for ZigBee, Thread and
Bluetooth are very popular and IAR EWARM is supported by NXP, TI,
SiLabs and others.

If you already have **IAR 7.40.2 for ARM** or later installed on
your Windows machine, you are fine.

IAR has a 30-day free trial. If you've already used a 30-day trial and can't
install it, call IAR and explain your are taking the UW IoT 210 course and
they should give you another 30-day free trial.

https://www.iar.com/iar-embedded-workbench/#!?currentTab=free-trials

Do NOT install the KickStart size-limited version, as it cannot build
the ZigBee programs. Choose the 30-day time-limited version instead.

# Step 5 - Download and Install Ubiqua Protocol Analyzer

If you got the Sniffer, or want to analyze packets, you can
use the following software.

www.ubilogix.com

Download 30-Day free trial




[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)
